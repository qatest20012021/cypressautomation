/// <reference types="Cypress" />
import CheckUKVisaPage from '../pageObjects/CheckUKVisaPage'
import TravelingWithPartner from '../pageObjects/TravelingWithPartner'
import VisaResultsPage from '../pageObjects/VisaResultsPage'


import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const checkUKVisaPage = new CheckUKVisaPage()
const travelingWithPartner= new TravelingWithPartner()
const visaResultsPage = new VisaResultsPage()


And('I am a Russia National', function () {

    checkUKVisaPage.selectCountry('russia')   

})

And('I am traveling without my partner', function () {
 
    travelingWithPartner.select('no')

})

And('I am traveling with my partner', function () {

    travelingWithPartner.select('yes')


})



