class TravelingWithPartner {

    travelingWithPartnerYes() {
        return cy.get('div.govuk-radios [value = "yes"]')

    }

    travelingWithPartnerNo() {
        return cy.get('div.govuk-radios [value = "no"]')

    }

    continueButton() {
        return cy.get('#current-question > .gem-c-button')

    }

    errorMessage() {
        return cy.get('.govuk-fieldset').contains('Please answer this question')
    }

    select(response) {

        switch (response) {
            case 'yes':
                this.travelingWithPartnerYes().check()
                this.continueButton().click()
                break;
            case 'no':
                this.travelingWithPartnerNo().check()
                this.continueButton().click()
                break;
            default:
                this.continueButton().click()

        }




    }

}
export default TravelingWithPartner;
