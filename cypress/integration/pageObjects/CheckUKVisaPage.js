class CheckUKVisaPage {

    begin() {

        cy.visit("https://www.gov.uk/check-uk-visa/y")
    }
    countryDropDown() {
        return cy.get('select[name="response"].govuk-select')
    }



    continueButton() {
        return cy.get('#current-question > .gem-c-button')

    }

    acceptCookies() {
        return cy.get('.gem-c-cookie-banner__button-accept > .gem-c-button')
    }




    selectCountry(country) {
        this.countryDropDown().select(country)
        this.acceptCookies().click()
        this.continueButton().click()
    }


}
export default CheckUKVisaPage;
