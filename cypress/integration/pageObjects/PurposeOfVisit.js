class PurposeOfVisit
{

responseTourism()
{
    return cy.get('div.govuk-radios [value = "tourism"]')
    
}

responseStudy()
{
    return cy.get('div.govuk-radios [value = "study"]')
    
}

continueButton()
{
    return cy.get('#current-question > .gem-c-button')

}

errorMessage()

{
    return cy.get('.govuk-fieldset').contains('Please answer this question')
}

selectPurposeOfVisit(purposeOfVisit)

{
    switch (purposeOfVisit) {
        case 'tourism':
          this.responseTourism().check()
          this.continueButton().click()
          break;
        case 'study':
            this.responseStudy().check()
            this.continueButton().click()
          break;
        default:
            this.continueButton().click()
                    
      }

}


}
export default PurposeOfVisit;
