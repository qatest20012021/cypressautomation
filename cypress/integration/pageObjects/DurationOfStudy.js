class DurationOfStudy {

    durationOfStudyLessThanSixMonths() {
        return cy.get('div.govuk-radios  [value="six_months_or_less"]')

    }

    durationOfStudyMoreThanSixMonths() {
        return cy.get('div.govuk-radios  [value="longer_than_six_months"]')

    }


    continueButton() {
        return cy.get('#current-question > .gem-c-button')

    }

    errorMessage() {
        return cy.get('.govuk-fieldset').contains('Please answer this question')
    }

    select(durationOfStudy) {
        switch (durationOfStudy) {
            case 'SixMonthsOrLess':
                this.durationOfStudyLessThanSixMonths().check()
                this.continueButton().click()
                break;
            case 'MoreThanSixMonths':
                this.durationOfStudyMoreThanSixMonths().check()
                this.continueButton().click()
                break;
            default:
                this.continueButton().click()

        }

    }

}
export default DurationOfStudy;
