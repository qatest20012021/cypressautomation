/// <reference types="Cypress" />

import PurposeOfVisit from '../pageObjects/PurposeOfVisit'
import VisaResultsPage from '../pageObjects/VisaResultsPage'
import CheckUKVisaPage from '../pageObjects/CheckUKVisaPage'

const purposeOfVisit = new PurposeOfVisit()
const visaResultsPage = new VisaResultsPage()
const checkUKVisaPage = new CheckUKVisaPage


Given('I want to visit the UK', () => {
    checkUKVisaPage.begin();
    
})

When('The purpose of my visit is Tourism', () => {

    purposeOfVisit.selectPurposeOfVisit('tourism')

})

Then('I do not need a Visa', () => {

    visaResultsPage.result().then((result) => {
        const resultText = result.text();
        expect(resultText).to.include('will not need a')
        cy.screenshot()

    })

})

Then('I do need a Visa', () => {

    visaResultsPage.result().then((result) => {
        const resultText = result.text();
        expect(resultText).to.not.include('will not need')
        cy.screenshot()


    })

})
