// Use this to get intellisense

/// <reference types ="Cypress" />

// Example usage axe-core via Cypress-axe.

describe('Accessibilty Tests with Cypress-axe', () => {
  var title = ''



  const axeLog = (violations) => {
    const violationData = violations.map(
      ({ id, impact, description, nodes }) => ({
        id,
        impact,
        description,
        nodes: nodes.length
      })
    )
    var resultFilePath = 'cypress/reports/accessibility/' + title + '.json'
    cy.writeFile(resultFilePath, JSON.stringify(violationData, null, "\t"))
    cy.log(JSON.stringify(violationData, null, "\t"))
  }

  it('Accessibility tests on Purpose of Visit Page', function () {
    cy.visit("https://www.gov.uk/check-uk-visa/y")
    cy.get('.gem-c-cookie-banner__button-accept > .gem-c-button').click()
    cy.get('select[name="response"].govuk-select').select('japan')
    cy.get('#current-question > .gem-c-button').click()
    cy.wait(500)
    cy.customInjectAxe()
    title = this.test.title.replace(/\s/g, '');
    cy.checkA11y(null, null, axeLog)

  })

  it('Accessibility tests on Purpose of Visit Page with aXe Filters', function () {
    cy.visit("https://www.gov.uk/check-uk-visa/y")
    cy.get('.gem-c-cookie-banner__button-accept > .gem-c-button').click()
    cy.get('select[name="response"].govuk-select').select('japan')
    cy.get('#current-question > .gem-c-button').click()
    cy.wait(500)
    cy.customInjectAxe()
    title = this.test.title.replace(/\s/g, '');
    cy.checkA11y(null, {
      runOnly: {
        type: 'tag',
        values: ['wcag21aa', 'wcag21a', 'wcag2aa', 'wcag2a']
      }
    }, axeLog)

  })

  it('Accessibility tests on landing page', function () {
    cy.visit("https://www.gov.uk/check-uk-visa/y")
    cy.wait(500)
    cy.customInjectAxe()
    title = this.test.title.replace(/\s/g, '');
    cy.checkA11y(null, {
      runOnly: {
        type: 'tag',
        values: ['wcag21aa', 'wcag21a', 'wcag2aa', 'wcag2a']
      }
    }, axeLog)

  })

})