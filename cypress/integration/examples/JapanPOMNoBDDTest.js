// Use this to get intellisense

/// <reference types ="Cypress" />

import CheckUKVisaPage from '../pageObjects/CheckUKVisaPage'
import DurationOfStudy from '../pageObjects/DurationOfStudy'
import PurposeOfVisit from '../pageObjects/PurposeOfVisit'
import VisaResultsPage from '../pageObjects/VisaResultsPage'

const checkUKVisaPage = new CheckUKVisaPage()
const visaResultsPage = new VisaResultsPage()
const purposeOfVisit = new PurposeOfVisit()
const durationOfStudy = new DurationOfStudy()


describe('Japan Test Feature. POM But no BDD', () => {
    
    //FF cookie issue
    afterEach(() => {
        cy.getCookies().then((cookies) => cookies.forEach(cookie => cy.clearCookie(cookie.name)));
      })


    it('Japanese National Wants to vist for Tourism', () => {
        
        checkUKVisaPage.begin()
        checkUKVisaPage.selectCountry('japan')  
        purposeOfVisit.selectPurposeOfVisit('tourism')
        visaResultsPage.result().then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('will not need a')
            cy.screenshot()

        })
            
    })

    it('Japanese National Wants to vist for Study less than 6 Months', () => {
        checkUKVisaPage.begin()
        checkUKVisaPage.selectCountry('japan') 
        purposeOfVisit.selectPurposeOfVisit('study')
        durationOfStudy.select('SixMonthsOrLess')
        visaResultsPage.result().then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('will not need a')
            cy.screenshot()

        })
     
    })

    it('Japanese National Wants to vist for Study more than 6 Months', () => {
        checkUKVisaPage.begin()
        checkUKVisaPage.selectCountry('japan') 
        purposeOfVisit.selectPurposeOfVisit('study')
        durationOfStudy.select('MoreThanSixMonths')

        visaResultsPage.result().then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('You’ll need a visa')
            cy.screenshot()

        })      
     
    })



  })