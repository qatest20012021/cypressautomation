// Use this to get intellisense

/// <reference types ="Cypress" />

// Example. Using axe-core directly with Cypress

describe('Inject axe-core and run it', function () {

  var filter = {
    runOnly: {
      type: 'tag',
      values: ['wcag21aa', 'wcag21a', 'wcag2aa', 'wcag2a']
    }
  }

  it('Runs axe-core', function () {
    var title = this.test.title.replace(/\s/g, '_');
    cy.axeCore('https://www.gov.uk/check-uk-visa/y/').then((results) => {
      cy.axeCoreResult(results, title)
    })
  })

  it('Runs axe-core with filters', function () {
    var title = this.test.title.replace(/\s/g, '-');

    cy.axeCore('https://www.gov.uk/check-uk-visa/y/', filter).then((results) => {
      cy.axeCoreResult(results, title)
    })
  })

  it('Runs axe-core with assertion', function () {
    var title = this.test.title.replace(/\s/g, '-');

    cy.axeCore('https://www.gov.uk/check-uk-visa/y/').then((results) => {
      cy.axeCoreResult(results, title).then(() => {
        expect(results.violations.length).to.equal(0)
      })

    })
  })

})