// Use this to get intellisense

/// <reference types ="Cypress" />


describe('API Tests', () => {


  it("GET", function () {
    cy.request({
      method: 'GET',
      failOnStatusCode: false,
      url: 'https://postman-echo.com/get?foo1=bar1&foo2=bar2'
    }).then((response) => {

      this.response_body = JSON.stringify(response.body)
      cy.log(this.response_body)
      cy.log(JSON.parse(this.response_body).args)
      cy.log(response.statusText)

      expect(response.status.toString()).to.equal('200')

    })
  })

  it("POST", function () {
    cy.request({
      method: 'POST',
      url: 'https://postman-echo.com/post',
      body: {
        message: "This is an example POST",
        method: "POST",
        comments: "You can pass the body from an object or a file too"
      }

    }).then((response) => {

      var response_body_data = JSON.stringify(response.body.data)
      var responde_body_data_message = JSON.stringify(response.body.data.message)
      cy.log(response_body_data)
      cy.log(responde_body_data_message)

      expect(response.status.toString()).to.equal('200')
      expect(responde_body_data_message).to.equal('\"This is an example POST\"')

    })
  })

  it("POST Second Example", function () {

    // Bulk post a request that fetches details of post codes
    // Verify for each post code that the query 
    cy.request({
      method: 'POST',
      url: 'http://postcodes.io/postcodes',
      body: {
        "postcodes": ["OX49 5NU", "M32 0JG", "NE30 1DP"]
      }

    }).then((response) => {

      var response_body_result = response.body.result
      var queryPostCodes = []
      var resultPostCodes = []

      for (var result in response_body_result) {
        queryPostCodes.push(response_body_result[result].query)
        resultPostCodes.push(response_body_result[result].result.postcode)
      }

      expect(response.status.toString()).to.equal('200')
      expect(queryPostCodes.sort()).to.have.ordered.members(resultPostCodes.sort())


      //recursively make GET calls and verify that individual potsode calls also return successs

      for (var postCode in queryPostCodes) {

        cy.request({
          method: 'GET',
          failOnStatusCode: false,
          url: 'http://postcodes.io/postcodes/' + queryPostCodes[postCode]
        }).then((response) => { expect(response.status.toString()).to.equal('200') })
      }


    })
  })


})