// Use this to get intellisense

/// <reference types ="Cypress" />
describe('Japan Test Feature. NO POM or BDD', () => {

    
    // Fixture example. Data Driven testing
    before(function() {
        cy.fixture('example').then(function(data){         
            this.testData = data            
        })
      })
    
    //FF Cookie issue
    afterEach(() => {
        cy.getCookies().then((cookies) => cookies.forEach(cookie => cy.clearCookie(cookie.name)));
      })

    
    it('Japanese National Wants to vist for Tourism', function()  {
        cy.visit("https://www.gov.uk/check-uk-visa/y")
        cy.get('.gem-c-cookie-banner__button-accept > .gem-c-button').click()
        cy.wait(500)       
        cy.get('select[name="response"].govuk-select').select(this.testData.countries[0])
        cy.get('#current-question > .gem-c-button').click()
        var radiobutton = 'div.govuk-radios [value =' + this.testData.purposeOfVisit[1] +  ']'
        cy.get(radiobutton).check()
        cy.get('#current-question > .gem-c-button').click()

        cy.get('.result-body > .gem-c-heading').then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('will not need a')
            cy.screenshot()

        })
        
     
    })

    it('Japanese National Wants to vist for Study less than 6 Months', () => {
        cy.visit("https://www.gov.uk/check-uk-visa/y")
        cy.get('.gem-c-cookie-banner__button-accept > .gem-c-button').click()
        cy.wait(500)       
        cy.get('select[name="response"].govuk-select').select('japan')
        cy.get('#current-question > .gem-c-button').click()
        cy.get('div.govuk-radios [value = "study"]').check()
        cy.get('#current-question > .gem-c-button').click()
        cy.get('div.govuk-radios  [value="six_months_or_less"]').check()
        cy.get('#current-question > .gem-c-button').click()

        cy.get('.result-body > .gem-c-heading').then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('will not need a')
            cy.screenshot()



        })
     
    })

    it('Japanese National Wants to vist for Study more than 6 Months', () => {
        cy.visit("https://www.gov.uk/check-uk-visa/y")
        cy.get('.gem-c-cookie-banner__button-accept > .gem-c-button').click()
        cy.wait(500)       
        cy.get('select[name="response"].govuk-select').select('japan')
        cy.get('#current-question > .gem-c-button').click()
        cy.get('div.govuk-radios [value = "study"]').check()
        cy.get('#current-question > .gem-c-button').click()
        cy.get('div.govuk-radios  [value="longer_than_six_months"]').check()
        cy.get('#current-question > .gem-c-button').click()

        cy.get('.result-body > .gem-c-heading').then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('You’ll need a visa')
            cy.screenshot()



        })
     
    })



  })