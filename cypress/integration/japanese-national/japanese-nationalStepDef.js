/// <reference types="Cypress" />
import CheckUKVisaPage from '../pageObjects/CheckUKVisaPage'
import DurationOfStudy from '../pageObjects/DurationOfStudy'
import PurposeOfVisit from '../pageObjects/PurposeOfVisit'
import VisaResultsPage from '../pageObjects/VisaResultsPage'


import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const checkUKVisaPage = new CheckUKVisaPage()
const visaResultsPage = new VisaResultsPage()
const purposeOfVisit = new PurposeOfVisit()
const durationOfStudy = new DurationOfStudy()



And('I am a Japan National', function () {

    checkUKVisaPage.selectCountry('japan')

})




When('The purpose of my visit is Study', () => {
    purposeOfVisit.selectPurposeOfVisit('study')

})

And(/^duration of my stay is (.*)$/, async function (duration) {

    if (duration == 'SixMonthsOrLess') {
        durationOfStudy.select('SixMonthsOrLess')
    } else {
        durationOfStudy.select('MoreThanSixMonths')
    }



});




Then(/^I (.*) need a Visa$/, (visaNeeded) => {

    if (visaNeeded == 'do not') {

        visaResultsPage.result().then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('will not need a')
            cy.screenshot()


        })

    } else {

        visaResultsPage.result().then((result) => {
            const resultText = result.text();
            expect(resultText).to.include('You’ll need a visa')
            cy.screenshot()


        })

    }


})
