@scenario=japan
Feature: Japanese national planning to visit UK checks Visa requirements
  
  Background: User accesses the UK Gov site 
	Given I want to visit the UK

   Scenario: Japanese national wants to visit UK for tourism
    
    And I am a Japan National
    When The purpose of my visit is Tourism    
    Then I do not need a Visa  
    
   
 Scenario Outline: Japanese national looking to visit UK for study 

 And I am a Japan National
 When The purpose of my visit is Study    
 And  duration of my stay is <Duration>
 Then I <VisaRequirement> need a Visa
	
	Examples: 
		|Duration       | VisaRequirement |     
		|SixMonthsOrLess|do not          |
		|MoreThanSixMonths|do       |