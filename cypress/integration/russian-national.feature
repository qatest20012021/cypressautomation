
Feature: Russian national planning to visit UK checks Visa requirements
  
  Background: User accesses the UK Gov site 
	Given I want to visit the UK
	And I am a Russia National
	When The purpose of my visit is Tourism
   
   @Russia
   Scenario: Russian national not traveling with partner   
            
    And I am traveling without my partner
    Then I do need a Visa    
   
   @Russia
   Scenario: Russian national traveling with partner. 
            
    And I am traveling with my partner
    Then I do need a Visa