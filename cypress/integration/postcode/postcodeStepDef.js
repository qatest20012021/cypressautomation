When(/^making a get request with (.*) I should get a (.*) response code/, async function (postCode, responseCode) {

    cy.request({
        method: 'GET',
        failOnStatusCode: false,
        url: 'http://api.postcodes.io/postcodes/' + postCode
    }).then((response) => {
        expect(response.status.toString()).to.equal(responseCode)

    })

});


