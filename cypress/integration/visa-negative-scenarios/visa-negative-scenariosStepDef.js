/// <reference types="Cypress" />
import CheckUKVisaPage from '../pageObjects/CheckUKVisaPage'
import DurationOfStudy from '../pageObjects/DurationOfStudy'
import PurposeOfVisit from '../pageObjects/PurposeOfVisit'
import TravelingWithPartner from '../pageObjects/TravelingWithPartner'



import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const checkUKVisaPage = new CheckUKVisaPage()
const purposeOfVisit = new PurposeOfVisit()
const durationOfStudy = new DurationOfStudy()
const travelingWithPartner= new TravelingWithPartner()




And('I am a Japan National', () => {

    checkUKVisaPage.selectCountry('japan')

})

And('I am a Russia National', () => {

    checkUKVisaPage.selectCountry('russia')

    
})

And('I do not select the purpose of my visit',()=>{

    purposeOfVisit.selectPurposeOfVisit()

})


And('The purpose of my visit is Study', () => {
    purposeOfVisit.selectPurposeOfVisit('study')

})

And('I do not select the duration of stay', () =>{

       durationOfStudy.select()

});

And('I do not select the partner question', () => {
 
     travelingWithPartner.select()

})

Then('I should see an error', () => {
 
    purposeOfVisit.errorMessage().should('be.visible')
    cy.screenshot()

})

Then('I should see an error on Duration of Stay Page', () => {
    
    durationOfStudy.errorMessage().should('be.visible')
    cy.screenshot()

})


Then('I should see an error on Traveling with partner question page', () => {
 
    travelingWithPartner.errorMessage().should('be.visible')
    cy.screenshot()

})







