// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//

Cypress.Commands.add('customInjectAxe', function () {
    cy.readFile('node_modules/axe-core/axe.min.js').then(function (source) {
        return cy.window({ log: false }).then(function (window) {
            window.eval(source);
        });
    });

})



Cypress.Commands.add('axeCoreResult', function (results, title) {
    const violationData = results.violations.map(
        ({ id, impact, description, nodes }) => ({
            id,
            impact,
            description,
            nodes: nodes.length
        })
    )

    if (results.violations.length > 0) {

        cy.log("accessibility violations")
        cy.log(JSON.stringify(violationData, null, "\t"))
        var resultFilePath = 'cypress/reports/accessibility/' + title + '_axeCore.json'
        cy.writeFile(resultFilePath, JSON.stringify(violationData, null, "\t"))


    }
    else {
        cy.log("No accessibility violations")
        var resultFilePath = 'cypress/reports/accessibility/' + title + '_axeCoreFullresult.json'
        cy.writeFile(resultFilePath, JSON.stringify(results, null, "\t"))
    }
   
})

Cypress.Commands.add('axeCore', function (url, filter){

    cy.visit(url)
      .window().then((win) => {
        var window = win;
        var axe = require('axe-core');
        window.eval(axe.source);
        return window.axe.run(filter);
      })

})


//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
