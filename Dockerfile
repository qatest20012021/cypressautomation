FROM cypress/browsers

WORKDIR '/app'

COPY ./package.json ./
RUN npm install

COPY ./ ./

CMD ["npx","cypress","run"]