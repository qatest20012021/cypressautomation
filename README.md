# CypressAutomation

This is a sample Project that demonstrates Cypress usage. The project contains examples that show a few different approaches to how Cypress can be used. 

# Examples include

## UI testing

1. BDD + POM approach
2. POM approach
3. no BDD or POM

## API testing 
1. Simple GET requests involving response body validation
2. Simple POST requests involving response body validation, recursive GET calls etc

## Accessibility

1. Axe-core and Cypress integration
   1. accessibilityTest.js has a sample implementation where axe-core is used via cypress-axe.
   
   2. accessibilityaXETest.js has a sample implementation where axe-core is directly used.

2. PA11y integration . Pending Implementation

## CI/CD

1. Gitlab default runners
2. Test execution using parallel runs and Dockers



# Requisites and installation
The Project will need Node version 12 or above.
Steps to install

1. Download or Clone the project.
2. run npm i to install depenancies.
3. npx cypress run should run all tests. Refer to '.gitlab-ci.yml'and 'package.json' for some more examples of how to set up shorter commands, pass filters,use glob patterns etc.



# Project Structure
 
## Code Files

1. Feature files are under 'cypress/Integration'.
2. Step Defintions for BDD tests are in folders named after features 'or' in 'common' folder
3. Non BDD tests are under 'cypress/Integration/examples' and must follow the naming pattern '*Test.js'. We can define any Glob pattern in the cypress.json
4. accessibility tests are under 'cypress/Integration/examples'and must follow the pattern '*Test.js'

## Reports
Project uses a few reporting options 
1. Mochawesome 
   creates individual report files for tests in 'cypress/mochawesome-report'

2. Mochawesome marge.

Following commands will produce the merged 'mochawesome.html' report in 'cypress/reports' folder
(use npx if you are on Windows)

   - '$(npm bin)/mochawesome-merge cypress/mochawesome-report/*.json > cypress/reports/mochawesome.json'
 
   - '$(npm bin)/marge cypress/reports/mochawesome.json --reportDir cypress/reports/ --inline'

3. Cucumber inbuilt json reports
   Reports generated for feature files only. Json reports can be found under
    'cypress/cucumber-json'

4. multiple-cucumber-html-reporter
   Reports generated for feature files only. HTML report needs following command.   
   
   'node cucumber-html-report.js'
   
   report can be found under 'cypress/reports/cucumber-htmlreport.html' folder as 'index.html'

5. Custom json report for accessibility tests
   These are created for each **failed** test only. Reports will be created with testname and found in teh folder 'reports/accessibility'.


# Dashboard and Slack Integration
The pipeline settings exports a key value pair for cypress dashboard use. Results of runs that have the '--record' parameter will send the results to the Cypress dashboard (Please reach out for access to the test Dashboard and Slack channel). Dashboard feature in Cypress helps with result analytics and consolidated reporting. Slack integration can also be acheived through the Dashboard. This has been implemented for this test project.

# BrowserStack Integration
Browser stack integration is acheived through the npm package 'browserstack-cypress-cli'.This is maintained by browserstack and does not appear in the list of cypress plugins.

Please note that the browserstack.json file will need to have npm_dependencies defined in run_settings (As provided here). run command "browserstack-cypress run" to run the tests via browserstack. 

_IMPORTANT NOTE - When the run command is invoked, it bundles the test files in a .zip file and uploads this to the browserstack account. Please discuss this within your projects to see if test code being uploaded to browserstack can be a potential issue.For this Project, at present this step is not icluded in CI/CD stage. This can be easily done by copying over one of the test statges.Browserstack is a paid subscription service._

# Pipeline Definition

## Outputs
Pipeline jobs which run test will have ouputs to download and will include 
1. Screensots
2. Reports
3. Videos (Thre are known issues with videos not capturing the entire duration of test run, especially when running in CI.)

## Stages

1. The first stage is an install stage. Ideally, this would be the AUT. In this example, we are just installing our npm test project. Note - this has been set to manual to save on runner time consumed (free runner time is limited). This can be set to auto trigger on checkin to Develop branch by removing the 'when' keyword from Gitlab CI YAML file.

2. Second stage is the Docker image build and run. This is provided as an example to be used if required. This again is set to manual and set to 'allow_failure' as true (will not fail the pipeline, if job fails). Also includes an example of using cucumber tags to filter tests. Note that the cucumber tags only apply to tests inside '.feature' file.

3. The Third Stage involves functional tests running in parallel. Note that the parallel running used here is via Gitlab Jobs. The test here also use Docker images provided by Cypress. However, this is used sligtly differently than in stage 2 (where a Docker file from the project itself is used to build the image). 

4. Stage 4 is the accessibility test making use of axe-core via cypress-axe. There are three sample axe tests. The first one does not consider any filters so axe-core tests for all rules. The second and the third tests pass on the filters 'wcag21aa', 'wcag21a', 'wcag2aa', 'wcag2a' to checkA11y(). The filter list to pass on would need inputs from accessibility testing expert in a real world project.


# Few things to note

## cypress-axe
1. There are known issues with installing cypress-axe. This project uses two work arounds to get around these. 

   1. Instead of the InjectAxe() function provided by cypress-axe, the project uses a custom Cypress custom command (defined as 'customInjectAxe' in 'cypress/support/commands.js'). This will prevent the require.resolve() related errors. Also serves as an example on how to use custom commands.

   2. A custom function axeLog() exists in the 'accessibilityTest.js' file. This is to overcome another failure seen when using cypress-axe. This also serves the purpose of writing the failure results to json file. There is scope to optimise the approach by making axeLog() available across the project but the function will need to implement the violations callback for checkA11y().

## cypress-cucumber-preprocessor
The project uses both feature files and js file tests. This hybrid mode works fine when calling spec files and also loading the cypress IDE and the command lines (with few tweaks) is able to interpret Glob defined in cypress.json. However, for some reason the existence of multiple test types in cypress.json defined by '"**/{*.feature,*Test.js}"' , seems to interfere with the ability of running tests using cucumber tags. The pattern is as per the  documentation available. This issue can be resolved by passing an array of individual Glob patterns. Refer to 'cypress.json' file and example provided in the Docker stage where a tag is being passed.

## mochawesome-merge and mochawesome
The reports created will include the feature file tests as well. The count of scenarios, statuses etc match up .However, the BDD steps within scenarios are not listed in the report and they will appear as Mocha code. Test failures will show up as code assertion failures rather than BDD step failure. This is still good to debug. The cucumber report is more likely to contain text in BDD format/style but only picks up feature file tests.


## cucumber-htmlreport
The merged report needs customisation and at present some of the values on the report (Browser, Device, OS etc are hardcoded).There is ofcourse a json report available and in an ideal set up this could be posted over to a test repository for result dashboarding and time series analysis.

